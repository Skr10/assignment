import dataclasses
from ShoppinBasket import Basket
import unittest


@dataclasses.dataclass()
class Item(object):
    unit_price = 100.0
    quantity = 1


class ShoppingBasketTest(unittest.TestCase):
    def test_empty_Basket_total(self):
        basket = Basket([])
        self.assertEqual(basket.total(), 0)

    def test_single_item_quantity(self):
        basket = Basket([Item()])
        self.assertEqual(basket.total(), 100.0)
